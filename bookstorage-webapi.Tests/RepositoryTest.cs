﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

using bookstorage_webapi.Controllers;
using bookstorage_webapi.Models;
using bookstorage_webapi.Repositories;

using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace bookstorage_webapi.Tests
{

    [TestFixture]
    public class RepositoryTest
    {
        BookRepository repo;


        
        [TestFixtureSetUp]
        public void Setup()
        {
            repo = new BookRepository();
            repo.RemoveDB();
            repo.AddBook(new Book("Kamenyar", "Ivan Franko"));
            repo.AddBook(new Book("Kobzar", "Taras Shevchenko"));
        }

        //Tests are run in alphabetical order of their names
        [Test]
        public void CheckAddBook()
        {
            repo.AddBook(new Book("Virshi", "Lesya Ukrainka"));
            Assert.AreEqual(repo.GetBooks()[repo.GetBooks().Count-1].Title, "Virshi");
            Assert.AreEqual(repo.GetBooks()[repo.GetBooks().Count - 1].Id, 3);
            Assert.AreEqual(repo.GetBooks().Count, 3);
        }

        [Test]
        public void CheckGetBook()
        {
            Assert.AreEqual(repo.GetBook(1).Author, "Ivan Franko");

        }

        [Test]
        public void CheckUpdateBook()
        {
            repo.UpdateBook(new Book(1,"Zahar Berkut", "Ivan Franko"));
            Assert.AreEqual(repo.GetBook(1).Title, "Zahar Berkut");
        }

        [Test]
        public void CheckGetAllBooks()
        {
            Assert.AreEqual(repo.GetBooks().Count, 2);
        }

        [Test]
        public void CheckDeleteBook()
        {
            repo.DeleteBook(3);
            foreach (Book b in repo.GetBooks())
            {
                Assert.AreNotEqual(b.Id, 3);
            }
            Assert.AreEqual(repo.GetBooks().Count, 2);
        }

        [TestFixtureTearDown]
        public void Close()
        {

        }

    }
}

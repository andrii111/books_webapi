﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace bookstorage_webapi.Models
{
    public class BookContext : DbContext
    {
        public BookContext()
            : base()
        {
            // Database.SetInitializer<BookContext>(null);
        }
        public DbSet<Book> Books { get; set; }
    }
}
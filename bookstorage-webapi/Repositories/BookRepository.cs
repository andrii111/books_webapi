﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Data.Entity;

using bookstorage_webapi.Models;

namespace bookstorage_webapi.Repositories
{
    public class BookRepository : IRepository<Book>  {

        BookContext context = new BookContext();

        public int Count()
        {
            return context.Books.ToList().Count;
        }

        public IList<Book> GetBooks()
        {
            return context.Books.ToList();
        }

        public IList<Book> GetBooks(String text)
        {
            IList<Book> books = context.Books.Where(e => e.Author.Contains(text) || e.Title.Contains(text)).ToList<Book>();
            return books;
        }

        public Book GetBook(int id)
        {
            return context.Books.Find(id);
        }

        public Book UpdateBook(Book item)
        {
            var book = context.Books.Find(item.Id);
            //context.Books.Attach(item);
            //context.Entry(item).State = EntityState.Modified;
            try
            {
                context.Entry(book).CurrentValues.SetValues(item);
            }
            catch (Exception e) { }
            context.SaveChanges();
            return book;
        }

        public Book AddBook(Book item)
        {
            context.Books.Add(item);
            context.SaveChanges();
            return item;
        }

        public Book DeleteBook(int id)
        {
            Book book = context.Books.Find(id);
            try
            {
                context.Books.Remove(book);
            }
            catch (Exception e) { }
            context.SaveChanges();
            return book;
        }

        public void RemoveDB()
        {
            context.Database.Delete();
        }

    }

}